# Project Name
> 

## Table of contents
* [General info](#general-info)
* [Screenshots](#screenshots)
* [Technologies](#technologies)
* [Setup](#setup)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
The educational project aimed at automating manual test scripts for the online store (http://automationpractice.com/index.php) learning and using tools for Continuous Integration and building your own test automation framework and Continuous Integration / Continuous Delivery process from scratch.
This project will give the chance to acquire skills desire for Software Development Engineer in Test.

## Screenshots


## Technologies
Python 

* Selenium WebDriver 

* Pytest 

* Allure 

* Logging 

* Xlrd  

GitLab  

Docker 

## Setup
Describe how to install / setup your local environement / add link to demo version.

## Code Examples
Show examples of usage:
`put-your-code-here`

## Features
List of features ready and TODOs for future development
* Awesome feature 1
* Awesome feature 2
* Awesome feature 3

To-do list:
* Wow improvement to be done 1
* Wow improvement to be done 2

## Status
Project is: in progress

## Inspiration
Add here credits. Project inspired by..., based on...

## Contact
Created by Julia Walczak & Martyna Klymov